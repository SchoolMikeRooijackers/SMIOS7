# SMIOS7

## Week 1
Developer account aangemaakt om samen te kunnen werken.
veel probelemen om certificaat te krijgen en healtkit te kunnen activeren.

## Week 2
scherm ontwerpen gemaakt. 
Flow in de storyboards gezet. 
Piechart met testdata werkend.
![alt tag](https://github.com/mikerooijackers/SMIOS7/blob/master/layout/iPhone-6S-Portrait-Mockup.png)
![alt tag](https://github.com/mikerooijackers/SMIOS7/blob/master/layout/iPhone-6S-Portrait-Mockup-2.png)
![alt tag](https://github.com/mikerooijackers/SMIOS7/blob/master/layout/iPhone-6S-Portrait-Mockup-3.png)

## Week 3
database ontwerp gemaakt.
database online gebracht.
tableview werkend met test data.
nog steeds werkt de healtkit certificaat niet.
ook zijn de devices nog niet toegevoegd.

![alt tag](https://github.com/mikerooijackers/SMIOS7/blob/master/diagram/database.png)